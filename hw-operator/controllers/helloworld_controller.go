/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	hwv1alpha1 "github.com/example-inc/hw-operator/api/v1alpha1"
	"github.com/go-logr/logr"
	routev1 "github.com/openshift/api/route/v1"
	"github.com/prometheus/common/log"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// HelloWorldReconciler reconciles a HelloWorld object
type HelloWorldReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=hw.example.com,resources=helloworldren,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=hw.example.com,resources=helloworldren/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=hw.example.com,resources=helloworldren/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=route.openshift.io,resources=routes,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;

func (r *HelloWorldReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	reqLogger := r.Log.WithValues("helloworld", req.NamespacedName)

	// Fetch the HelloWorld instance
	instance := &hwv1alpha1.HelloWorld{}
	wasFoundErr := r.Client.Get(ctx, req.NamespacedName, instance)

	// The instance was not found
	if wasFoundErr != nil {
		if errors.IsNotFound(wasFoundErr) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			log.Info("HelloWorld instance as not found. Not requeuing since object must have been deleted.")
			return ctrl.Result{}, nil
		}

		// Error getting the object, requeue the request
		log.Error("Failed to get HelloWorld instance")
		return ctrl.Result{}, wasFoundErr
	}

	result, wasFoundErr := r.ensureDeployment(req, instance, r.backendDeployment(instance))

	// Enters if there was a change or an error
	if result != nil {
		return *result, wasFoundErr
	}

	result, wasFoundErr = r.ensureService(req, instance, r.backendService(instance))

	// Enters if there was a change or an error
	if result != nil {
		return *result, wasFoundErr
	}

	result, wasFoundErr = r.ensureRoute(req, instance, r.backendRoute(instance))

	// Enters if there was a change or an error
	if result != nil {
		return *result, wasFoundErr
	}

	// Objects already exist
	reqLogger.Info("Deployment, service and route already exist")

	return ctrl.Result{}, nil
}

func (r *HelloWorldReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&hwv1alpha1.HelloWorld{}).
		Owns(&appsv1.Deployment{}).
		Owns(&corev1.Service{}).
		Owns(&routev1.Route{}).
		Complete(r)
}

// Assign labels
func labels(v *hwv1alpha1.HelloWorld, tier string) map[string]string {
	return map[string]string{
		"app":             "visitors",
		"visitorssite_cr": v.Name,
		"tier":            tier,
	}
}

// Builds a deployment and exposes a port at 8080
func (r *HelloWorldReconciler) backendDeployment(v *hwv1alpha1.HelloWorld) *appsv1.Deployment {
	labels := labels(v, "backend")
	size := v.Spec.Size

	dep := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      v.Name + "-deployment",
			Namespace: v.Namespace,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &size,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Image:           v.Spec.Image,
						ImagePullPolicy: corev1.PullAlways,
						Name:            "hello-pod",
						Ports: []corev1.ContainerPort{{
							ContainerPort: 8080,
							Name:          "hello",
						}},
					}},
				},
			},
		},
	}

	controllerutil.SetControllerReference(v, dep, r.Scheme)
	return dep
}

// Builds a service
func (r *HelloWorldReconciler) backendService(v *hwv1alpha1.HelloWorld) *corev1.Service {
	labels := labels(v, "backend")

	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      v.Name + "-svc",
			Namespace: v.Namespace,
		},
		Spec: corev1.ServiceSpec{
			Selector: labels,
			Ports: []corev1.ServicePort{{
				Name:       "hello-port",
				Protocol:   corev1.ProtocolTCP,
				Port:       80,
				TargetPort: intstr.FromInt(8080),
			}},
			Type: corev1.ServiceTypeNodePort,
		},
	}

	controllerutil.SetControllerReference(v, svc, r.Scheme)
	return svc

}

// Builds a route
func (r *HelloWorldReconciler) backendRoute(v *hwv1alpha1.HelloWorld) *routev1.Route {
	route := &routev1.Route{
		ObjectMeta: metav1.ObjectMeta{
			Name:      v.Name + "-route",
			Namespace: v.Namespace,
		},
		Spec: routev1.RouteSpec{
			To: routev1.RouteTargetReference{
				Name: v.Name + "-svc",
				Kind: "Service",
			},
		},
	}

	controllerutil.SetControllerReference(v, route, r.Scheme)
	return route
}

// Checks if deployment already exists. Otherwise, creates a deployment
func (r *HelloWorldReconciler) ensureDeployment(request ctrl.Request,
	instance *hwv1alpha1.HelloWorld,
	dep *appsv1.Deployment) (*ctrl.Result, error) {

	foundDep := &appsv1.Deployment{}
	wasFoundErr := r.Client.Get(context.TODO(), types.NamespacedName{
		Name:      dep.Name,
		Namespace: instance.Namespace,
	}, foundDep)

	// If deployment wasn't found but there were no other errors
	if wasFoundErr != nil && errors.IsNotFound(wasFoundErr) {

		// Creates the deployment
		log.Info("Creating a new deployment ", "Deployment.Namespace ", dep.Namespace, "Deployment.Name ", dep.Name)
		err := r.Client.Create(context.TODO(), dep)

		// Checks if deployment has been created
		if err != nil {
			// Deployment has failed
			log.Error(err, "Failed to create a new deployment", "Deployment.Namespace ", dep.Namespace, "Deployment.Name ", dep.Name)
			return &ctrl.Result{}, err
		}

		// Deployment was successful
		return &ctrl.Result{Requeue: true}, nil

	} else if wasFoundErr != nil {
		// Error that isn't due to the deployment not existing
		log.Error("Failed to get deployment", wasFoundErr)
		return &ctrl.Result{}, wasFoundErr
	} else {
		// Deployment exists. Checks Spec
		// Ensures the delpoyment size is the same as the spec
		size := instance.Spec.Size
		if *foundDep.Spec.Replicas != size {
			foundDep.Spec.Replicas = &size
			err := r.Update(context.TODO(), foundDep)

			if err != nil {
				log.Error("Failed to update deployment", "Deployment.Namespace", foundDep.Namespace, "Deployment.Name", foundDep.Name)
				return &ctrl.Result{}, err
			}

			// Spec updated, return and requeue
			return &ctrl.Result{Requeue: true}, nil
		}
	}

	return nil, nil
}

func (r *HelloWorldReconciler) ensureService(request ctrl.Request,
	instance *hwv1alpha1.HelloWorld,
	svc *corev1.Service) (*ctrl.Result, error) {

	foundSvc := &corev1.Service{}
	wasFoundErr := r.Client.Get(context.TODO(), types.NamespacedName{
		Name:      svc.Name,
		Namespace: instance.Namespace,
	}, foundSvc)

	// If service wasn't found but there were no other errors
	if wasFoundErr != nil && errors.IsNotFound(wasFoundErr) {

		// Creates the service
		log.Info("Creating a new service ", "Service.Namespace ", svc.Namespace, "Service.Name ", svc.Name)
		err := r.Client.Create(context.TODO(), svc)

		// Checks if service has been created
		if err != nil {
			// Service has failed to be created
			log.Error(err, "Failed to create a new Service", "Service.Namespace ", svc.Namespace, "Service.Name ", svc.Name)
			return &ctrl.Result{}, err
		}
		// Service has been created
		return &ctrl.Result{Requeue: true}, nil

	} else if wasFoundErr != nil {
		// Error that isn't due to the service not existing
		log.Error("Failed to get service", wasFoundErr)
		return &ctrl.Result{}, wasFoundErr

	}

	return nil, nil
}

func (r *HelloWorldReconciler) ensureRoute(request ctrl.Request,
	instance *hwv1alpha1.HelloWorld,
	route *routev1.Route) (*ctrl.Result, error) {

	foundRoute := &routev1.Route{}
	wasFoundErr := r.Client.Get(context.TODO(), types.NamespacedName{
		Name:      route.Name,
		Namespace: instance.Namespace,
	}, foundRoute)

	// If route wasn't found but there were no other errors
	if wasFoundErr != nil && errors.IsNotFound(wasFoundErr) {

		// Creates the route
		log.Info("Creating a new route ", " Route.Namespace ", route.Namespace, " Route.Name ", route.Name)
		err := r.Client.Create(context.TODO(), route)

		// Checks if route has been created
		if err != nil {
			// Route has failed to be created
			log.Error(err, "Failed to create a new Route", " Route.Namespace ", route.Namespace, " Route.Name ", route.Name)
			return &ctrl.Result{}, err
		}

		// Route has been created
		return &ctrl.Result{Requeue: true}, nil
	} else if wasFoundErr != nil {
		// Error that isn't due to the route not being found
		log.Error("Failed to get route", wasFoundErr)
		return &ctrl.Result{}, wasFoundErr
	}

	return nil, nil
}
